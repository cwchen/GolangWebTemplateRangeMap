package main

import (
	"html/template"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func main() {
	mux := httprouter.New()

	mux.GET("/", index)

	server := http.Server{
		Addr:    "localhost:8080",
		Handler: mux,
	}

	server.ListenAndServe()
}

func index(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	var tmpl = template.Must(template.ParseFiles("views/index.html"))

	tmpl.Execute(w, struct {
		Frameworks map[string]string
	}{
		map[string]string{
			"Django":  "Python",
			"Rails":   "Ruby",
			"Laravel": "PHP",
			"Spring":  "Java",
			"Gin":     "Golang",
		},
	})
}
