# [Golang][Web] `range` with map in http/template

A tiny demo app for `range` with map in [http/template](https://golang.org/pkg/html/template/).

## Usage

Use [Git](https://git-scm.com/) to clone this repo:

```
$ git clone https://gitlab.com/cwchen/GolangWebTemplateRangeMap.git
```

Run this program:

```
$ cd GolangWebTemplateRangeMap
$ go run main.go
```

Visit http://localhost:8080 for the result:

![Using range with map in http/template](images/golang-web-template-range-map.PNG)

## Copyright

2018, Michael Chen; Apache 2.0.
